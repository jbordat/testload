package simulation

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration._

class ViewUsers{

  val scenarioReceiver = scenario("View User")
  .exec(http("Get User List")
    .get("/api/users")
    .header("Content-Type","application/json")
    .check(status is 200))

  def getViewUserRequest: ScenarioBuilder = scenarioReceiver
}