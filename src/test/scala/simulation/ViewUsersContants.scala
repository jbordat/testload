package simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class ViewUsersContants extends Simulation {

  val httpProtocol = http.baseUrl(System.getProperty("Host", "https://reqres.in"))
  val viewUsers = new ViewUsers

  setUp(viewUsers.getViewUserRequest.inject(atOnceUsers(100)).protocols(httpProtocol))
}
